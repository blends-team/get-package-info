# Description: Provide some information about packages by querying apt-cache
# Author: Andreas Tille <tille@debian.org>
# License: GPL

import apt
from sys import stderr
import re

cache = apt.Cache()

def get_pkg_version(package):
    try:
        pkg = cache[package] # Access the Package object for python-apt
    except KeyError:
        print("Unknown package name", package, file=stderr)
        return None

    version = pkg.installed
    if version:
        return version.source_version
    else:
        # print(package, "is not installed")
        return None

def get_upstream_version(package):
    pkg_version = get_pkg_version(package)
    if pkg_version:
        return re.sub('^\d+:','',re.sub('\+[dfsg]+$','', re.sub('-\d+[^-]*$','',pkg_version)))
    else:
        return None

def print_pkg_info(package):
    try:
        pkg = cache[package] # Access the Package object for python-apt
    except KeyError:
        print("Unknown package name", package, file=stderr)
        return

    version = pkg.installed
    if version:
        print('%s (%s): %s' % (package, version.source_version, pkg.candidate.summary))
        bins=[]
        for bin in pkg.installed_files:
            if bin.startswith('/usr/bin/') or bin.startswith('/bin/'):
                bins.append(bin)
        for bin in bins:
            print("	", bin)
    else:
        print(package, "(not installed)")

